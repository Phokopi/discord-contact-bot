const Discord = require('discord.js');
const fs = require('fs');
const config = require('./config.js');
const { discordToken } = require('./config.json');

const loadCommands = () => {
    const commands = {};
    const files = fs.readdirSync('./commands');

    files.forEach((file) => {
        if (file.endsWith('.js')) {
            commands[file.slice(0, -3)] = require(`./commands/${file}`);
            console.log(`Loaded ${file}`);
        }
    });

    return commands;
};

const client = new Discord.Client();
client.config = config;
client.commands = loadCommands();

client.once('ready', () => {
    client.user.setActivity(client.config.activityMessage).catch(console.error);
});

client.on('message', (message) => {
    // Ignore message if it does not begin with prefix, or if sent by a bot
    if (message.author.bot || !message.content.startsWith(client.config.prefix)) {
        return;
    }

    // Split the message and retrieve the command
    const args = message.content.slice(client.config.prefix.length).trim().split(/ +/g);
    const command = args.shift().toLowerCase();

    // Check if the provided command exists
    if (Object.keys(client.commands).includes(command)) {
        client.commands[command].run(client, message, args);
    }
});

client.login(discordToken);
