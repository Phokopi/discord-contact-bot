const contact = {
    name: 'contact',
    category: 'Information',
    description: 'Displays information about a teacher matching **all** the provided keywords.',
    usage: 'contact <keyword1> [<keyword2> <keyword3> ... <keywordn>]',
};

const iiens = {
    name: 'iiens',
    category: 'Information',
    description: 'Displays information about a student partially matching **all** the provided keywords.',
    usage: 'iiens <keyword1> [<keyword2> <keyword3> ... <keywordn>]',
};

const help = {
    name: 'help',
    category: 'System',
    description: 'Displays all the available commands.',
    usage: 'help [<command>]',
};

module.exports = {
    contact,
    iiens,
    help,
};
