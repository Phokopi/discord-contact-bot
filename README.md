# Install modules listed as dependencies in package.json

- `npm install` / `yarn` when developing.
- `npm install --production` / `yarn --prod` when using the bot in a production environment.

# Pre-requirements

- You need a `config.json` file in this folder. See `config.json.example` for a file example :
    + Provide a token in *discordToken*
    + Provide an URL to retrieve the CSV for teachers in *teachersCsvUrl*
    + Provide the file path of the CSV for students in *studentsFilePath*
    + [Optional] Provide a custom prefix in *commandPrefix*
    + [Optional] Provide a *name* and/or *picture* in *embedConfig*
- You need a trombi file in this folder. You need to put information about students/iiens inside of it, respecting the format in `trombi.csv.example`.
    + This is currently done by going to each promotion search page on [iiens.net](https://www.iiens.net), and Ctrl + S saving the full HTML page.
    + A oneshot `trombi-scrapper.py` script is provided to format data scrapped as a `.csv` file.

# Launch the bot

Once you've done the two steps above, start the bot by typing `node index.js` / `yarn start`.
