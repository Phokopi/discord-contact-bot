const chai = require('chai');
const sinon = require('sinon');
const sinonChai = require('sinon-chai');

const should = chai.should();
chai.use(sinonChai);

const csvHandler = require('../../utils/csv-handler');
const request = require('request-promise');
const csv = require('csvtojson');

describe('CSV Handler', () => {
    const fakeCsvString = '';
    const fakeCsvFile = '';
    const fakeCsvUrl = '';

    const tests = [
        {
            param: fakeCsvString,
            method: csvHandler.csvStringInformationAsArray,
            source: 'string',
        },
        {
            param: fakeCsvFile,
            method: csvHandler.csvStringInformationAsArray,
            source: 'file',
        },
        {
            param: fakeCsvUrl,
            method: csvHandler.csvUrlInformationAsArray,
            source: 'URL',
        },
    ];

    const fakeCsvOptions = {};
    const fakeCsvArrayResult = [{ obj: 'value' }];

    afterEach(() => {
        sinon.restore();
    });

    describe('Successfully read CSV informations', () => {
        beforeEach(() => {
            sinon
                .stub(csv.Converter.prototype, 'fromString')
                .resolves(fakeCsvArrayResult);
            sinon
                .stub(csv.Converter.prototype, 'fromFile')
                .resolves(fakeCsvArrayResult);
            sinon.stub(request, 'get').resolves(fakeCsvString);
        });

        tests.forEach(async ({ param, method, source }) => {
            it(`Returns an array when reading CSV from ${source}`, async () => {
                const csvResult = await method(param, fakeCsvOptions);
                csvResult.should.deep.equal(fakeCsvArrayResult);
            });
        });
    });

    describe('Handle error when reading CSV informations', () => {
        beforeEach(() => {
            sinon.stub(csv.Converter.prototype, 'fromString').throws();
            sinon.stub(csv.Converter.prototype, 'fromFile').throws();
            sinon.stub(request, 'get').throws();
        });

        tests.forEach(async ({ param, method, source }) => {
            it(`Returns null when reading CSV from ${source} fails`, async () => {
                await (async () => {
                    const csvResult = await method(param, fakeCsvOptions);
                    should.equal(csvResult, null);
                });
            });
        });
    });
});
