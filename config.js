const DEFAULT_PREFIX = '?';
const { commandPrefix = DEFAULT_PREFIX, embedConfig } = require('./config.json');

module.exports = {
    prefix: commandPrefix,
    activityMessage: `${commandPrefix}help`,
    embedConfig: {
        name: embedConfig.name,
        picture: embedConfig.picture,
    },
};
