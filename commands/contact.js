const { teachersCsvUrl } = require('../config.json');
const { csvUrlInformationAsArray } = require('../utils/csv-handler');

const helpMessages = require('../constants/help-messages');
const TeacherCommand = require('./classes/teacher-command');

const headers = [
    'keywords',
    'responsabilite',
    'nom',
    'salle',
    'tel',
    'mail',
    'other',
];

const csvOptions = {
    headers,
};
const teachersFromCsv = csvUrlInformationAsArray(teachersCsvUrl, csvOptions);

exports.run = async (client, message, args) => {
    const command = new TeacherCommand(0x70da75, await teachersFromCsv);
    command.run(client, message, args);
};

exports.help = helpMessages.contact;
