const EntityCommand = require("./entity-command");
const Teacher = require("../../utils/entity/teacher");

class TeacherCommand extends EntityCommand {
    constructor(embedColor, csvSource) {
        super(embedColor);
        this.entities = csvSource.map(infos => new Teacher(infos));
    }
}

module.exports = TeacherCommand;
