const { discordEmbed, pushFieldToEmbed } = require("../../utils/discord-embed");
const removeDiacritics = require("diacritics").remove;

const maxResponses = 5;

class EntityCommand {
    constructor(embedColor) {
        this.embedColor = embedColor;
    }

    noArgumentsResponse(message) {
        this.logAnswered(message.reply("5, 4, 3, 2, 1... Rhododendron !"));
    }

    pushFieldsToEmbed(args) {
        this.entities.forEach(entity => {
            if (entity.checkArgumentsMatchEntity(args)) {
                pushFieldToEmbed(entity.embedField, this.embed);
            }
        });
    }

    sendResponse(message) {
        let replyMessage = null;

        if (this.embed.fields.length > maxResponses) {
            replyMessage = `ta requête renvoie plus de ${maxResponses} résultats. Voici un aperçu des résultats :`;
        } else if (this.embed.fields.length > 0) {
            replyMessage = "voici le résultat de ta requête :";
        } else {
            replyMessage = "ta requête ne renvoie aucun résultat.";
        }

        while (this.embed.fields.length > maxResponses) {
            this.embed.fields.shift();
        }

        this.logAnswered(message,
            message.reply(replyMessage, {
                embed: this.embed.fields.length > 0 ? this.embed : null
            })
        );
    }

    logAsked(message) {
        console.log(
            `${message.author.username} asked for : ${message.content}`
        );
    }

    logAnswered(message, discordPromise) {
        return discordPromise
            .then(() => {
                console.log(`Sent a reply to ${message.author.username}`);
            })
            .catch(console.error);
    }

    run(client, message, args) {
        this.embed = discordEmbed(this.embedColor, []);

        const argsLowerCased = args.map(arg =>
            removeDiacritics(arg.toLowerCase())
        );

        this.logAsked(message);

        if (args.length === 0) {
            this.noArgumentsResponse(message);
        } else {
            this.pushFieldsToEmbed(argsLowerCased);
            this.sendResponse(message);
        }
    }
}

module.exports = EntityCommand;
