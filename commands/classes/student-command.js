const EntityCommand = require("./entity-command");
const Student = require("../../utils/entity/student");

class StudentCommand extends EntityCommand {
    constructor(embedColor, csvSource) {
        super(embedColor);
        this.entities = csvSource.map(infos => new Student(infos));
    }
}

module.exports = StudentCommand;
