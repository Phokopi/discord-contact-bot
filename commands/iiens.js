const { studentsFilePath } = require('../config.json');
const { csvFileInformationAsArray } = require('../utils/csv-handler');

const helpMessages = require('../constants/help-messages');
const StudentCommand = require('./classes/student-command');

const headers = [
    'prenom',
    'pseudo',
    'nom',
    'promo',
    'photo',
    'etablissement',
    'filiere',
    'naissance',
    'portable',
    'fixe',
    'mailEnsiie',
    'mailPerso',
    'antenne',
    'residence',
    'appartement',
    'adresse',
    'groupe',
    'citation',
    'associations',
    'inutile',
];

const csvOptions = {
    headers,
    delimiter: '§',
};
const studentsFromCsv = csvFileInformationAsArray(studentsFilePath, csvOptions);

exports.run = async (client, message, args) => {
    const command = new StudentCommand(0x71b5d9, await studentsFromCsv);
    command.run(client, message, args);
};

exports.help = helpMessages.iiens;
