from bs4 import BeautifulSoup
import csv
import unicodedata
import string
import re

def get_nom_pseudo_prenom(full_name):
    if full_name.count("'") >= 2:
        # Premier split : prénom
        first_split = list(filter(None, full_name.split(" '", 1)))
        prenom = first_split[0]

        # Second split : pseudo et nom
        second_split = list(filter(None, first_split[1].split("' ")))
        pseudo = "'".join(second_split[0:len(second_split) - 1])
        nom = second_split[len(second_split) - 1]
    else:
        full_name_split = full_name.split(" ")
        prenom, pseudo, nom = "", "", ""
        for split in full_name_split:
            if sum(1 for c in split if c.islower()) == 0:
                nom += split + " "
            else:
                prenom += split + " "

    return prenom.strip(), pseudo.strip(), nom.strip().replace('D"', "D'")


def remove_accents(s):
    return ''.join(x for x in unicodedata.normalize('NFKD', s)
                   if x in string.ascii_letters).lower()

LABEL_VALUES = \
    ["Prénom", "Pseudo", "Nom", "Promo", "Photo",
     "Établissement d'origine", "Filière", "Date de naissance",
     "Téléphone (port)", "Téléphone (fixe)", "Mail ensiie",
     "Mail perso", "Etudie à", "Résidence", "Appartement",
     "Adresse", "Groupe", "Citation", "Associations"
     ]

LABEL_KEYS = \
    ["prenom", "pseudo", "nom", "promo", "photo_url",
     "etablissement_origine", "filiere", "naissance",
     "tel_portable", "tel_fixe", "mail_ensiie",
     "mail_perso", "antenne", "residence", "appartement",
     "adresse", "groupe", "citation", "associations"
     ]

MIN_PROMO = 2002
MAX_PROMO = 2020
DESTINATION = 'trombi.csv'

if __name__ == "__main__":
    output = []

    for annee in range(MIN_PROMO, MAX_PROMO+1):
        print(annee)

        with open('trombi_{}.html'.format(annee), 'r', encoding="utf-8") as f:
            soup = BeautifulSoup(f.read(), 'html.parser')

        etudiants = soup.find_all('div', class_='page')

        for etu_num, etudiant in enumerate(etudiants):
            # Current row being worked on
            c_row = {}
            for key in LABEL_KEYS:
                c_row[key] = ""

            # Prénom, pseudo et nom
            full_name = etudiant.find('td', class_='nom').get_text()
            c_row["prenom"], c_row["pseudo"], c_row["nom"] = \
                get_nom_pseudo_prenom(full_name)

            # Année de promotion
            promo = etudiant.find('td', class_='promo').get_text()
            c_row["promo"] = int(promo.split(" ")[1])

            # URL absolu vers la photo du trombi
            photo_url = etudiant.find('td', class_='photo').select('img')[0]['src']
            c_row["photo_url"] = 'https://www.iiens.net/' + photo_url

            # Autres informations
            infos = etudiant.find('td', class_='infos').select('tr')

            for info in infos:
                key_td = info.find('td', class_='key')
                val_td = info.find('td', class_='val')
                cit_td = info.find('td', class_='cit')
                if key_td is not None and val_td is not None:
                    label = key_td.get_text().rstrip()
                    value = val_td.get_text().rstrip()
                    if re.search(re.compile('Associations'), label):
                        l = [v.string for v in val_td.find_all('li') if v.string is not None]
                        value = '@'.join(l)
                elif cit_td is not None:
                    label = "Citation"
                    value = cit_td.get_text().rstrip()
                else:
                    continue

                for index, label_name in enumerate(LABEL_VALUES):
                    if remove_accents(label_name) in remove_accents(label):
                        c_row[LABEL_KEYS[index]] = value.replace("\n", " ")

            output.append(c_row)

    # Save as a csv
    with open(DESTINATION, 'w', newline='', encoding="utf-8") as f:
        w = csv.DictWriter(f, LABEL_KEYS, delimiter='§')
        w.writeheader()
        for row in output:
            w.writerow({label: row.get(label) or "" for label in LABEL_KEYS})
