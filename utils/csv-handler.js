const csv = require('csvtojson');
const request = require('request-promise');

const csvStringInformationAsArray = async (csvInformationString, options) => {
    try {
        return await csv({ ...options }).fromString(csvInformationString);
    }
    catch (error) {
        console.error(error);
        return null;
    }
};

const csvFileInformationAsArray = async (csvInformationFile, options) => {
    try {
        return await csv({ ...options }).fromFile(csvInformationFile);
    }
    catch (error) {
        console.error(error);
        return null;
    }
};

const csvUrlInformationAsArray = async (csvInformationUrl, options) => {
    try {
        return csvStringInformationAsArray(
            await request.get(csvInformationUrl),
            options,
        );
    }
    catch (error) {
        console.error(error);
        return null;
    }
};

module.exports = {
    csvStringInformationAsArray,
    csvFileInformationAsArray,
    csvUrlInformationAsArray,
};
