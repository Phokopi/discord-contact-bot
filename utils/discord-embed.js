const { embedConfig: { name, picture } } = require('../config.js');

const Discord = require('discord.js');

const discordEmbed = (color, fields) =>
    new Discord.RichEmbed({
        color,
        fields,
        footer: {
            text: name,
            icon_url: picture,
        },
    });

const pushFieldToEmbed = (field, embed) => {
    embed.fields.push(field);
};

module.exports = {
    discordEmbed,
    pushFieldToEmbed,
};
