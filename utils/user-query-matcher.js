const checkAllArgumentMatchAnyEntityKeyword = (args, entity) => args.every(arg => entity.keywords.includes(arg.toLowerCase()));

module.exports = {
    checkAllArgumentMatchAnyEntityKeyword,
};
