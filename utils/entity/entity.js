const { checkAllArgumentMatchAnyEntityKeyword } = require('../user-query-matcher');

const removeDiacritics = require('diacritics').remove;

const formatForSearch = string =>
    removeDiacritics(string.toLowerCase()).replace(/[^a-zA-Z\d\s]+/g, ' ');

class Entity {
    constructor(infos, keywordFields) {
        const self = this;
        Object.entries(infos).forEach(([key, value]) => {
            self[key] = value;
        });

        this.keywords = []
            .concat(...keywordFields.map(fieldName =>
                formatForSearch(this[fieldName]).split(' ')))
            .filter(keyword => keyword.length > 1);
    }

    checkArgumentsMatchEntity(args) {
        return checkAllArgumentMatchAnyEntityKeyword(args, this);
    }
}

module.exports = Entity;
