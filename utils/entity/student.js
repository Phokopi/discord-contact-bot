const Entity = require('./entity');

class Student extends Entity {
    constructor(infos) {
        const keywordFields = ['prenom', 'pseudo', 'nom', 'promo', 'portable'];
        super(infos, keywordFields);
    }

    entityToString() {
        let ret = '';

        if (this.promo && this.antenne) {
            ret += `Promo ${this.promo} [${this.antenne}]`;
        }
        else if (this.promo) {
            ret += `Promo ${this.promo}`;
        }

        ret += ` - [Profil](https://www.iiens.net/etudiants/trombi.php?nom=${this.nom.replace(
            /\s/g,
            '%20',
        )}&prenom=${this.prenom.replace(/\s/g, '%20')}&promoAnnee[]=${
            this.promo
        }&tsub)`;

        // if (this.portable) {
        //   ret += '\n- ☎️ : '+this.portable;
        // }

        // if (this.mailEnsiie) {
        //   ret += '\n- ['+this.mailEnsiie+'](mailto:'+this.mailEnsiie+')';
        // }

        // if (this.mailPerso) {
        //   ret += '\n- ['+this.mailPerso+'](mailto:'+this.mailPerso+')';
        // }

        return ret;
    }

    get embedField() {
        let title = '●';
        if (this.prenom) {
            title += ` ${this.prenom}`;
        }
        if (this.pseudo) {
            title += ` '${this.pseudo}'`;
        }
        if (this.nom) {
            title += ` ${this.nom}`;
        }
        if (this.promo) {
            title += ` [${this.promo}]`;
        }

        return {
            name: title,
            value: this.entityToString(),
            inline: false,
        };
    }
}

module.exports = Student;
