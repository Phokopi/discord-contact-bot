const Entity = require('./entity');

class Teacher extends Entity {
    constructor(infos) {
        const keywordFields = ['keywords', 'responsabilite', 'nom', 'mail'];
        super(infos, keywordFields);
    }

    entityToString() {
        let ret = '';

        if (this.nom) {
            // ret += ' - '+this.nom;
            ret += this.nom;
        }
        if (this.salle) {
            ret += ` - Bur. ${this.salle}`;
        }
        if (this.tel) {
            ret += ` - ☎️ : ${this.tel}`;
        }
        if (this.mail) {
            // remplace les espaces (tous grâce à \s) '' (donc enlève les espaces), puis sépare selon virgule
            const mailArray = this.mail.replace(/\s/g, '').split(',');
            for (let loop = 0; loop < mailArray.length; loop++) {
                ret += ` - [${mailArray[loop]}](mailto:${mailArray[loop]})`;
            }
        }
        if (this.other) {
            ret += ` [${this.other}]`;
        }

        return ret;
    }

    get embedField() {
        return {
            name: `● ${this.responsabilite}`,
            value: this.entityToString(),
        };
    }
}

module.exports = Teacher;
